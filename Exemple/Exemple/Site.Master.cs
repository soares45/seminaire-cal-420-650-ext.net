﻿using System;
using System.Web.UI;
using Ext.Net;

namespace Exemple
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnHome_Click(object sender, DirectEventArgs e)
        {
            Response.Redirect("Default.aspx", true);
        }

        protected void BtnForm_Click(object sender, DirectEventArgs e)
        {
            Response.Redirect("ExempleForm.aspx", true);
        }

        protected void BtnGrid_Click(object sender, DirectEventArgs e)
        {
            Response.Redirect("ExempleGrid.aspx", true);
        }
    }
}